// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

const path = require('path');

/**
 * Configuration factory, can be used in subproject configurations.
 */
function configFactory(projectName) {
  const reportDir = path.join(__dirname, 'reports');
  return (config) => config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-spec-reporter'),
      require('karma-jasmine-html-reporter'),
      require('karma-junit-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      dir: path.join(__dirname, '.nyc_output'),
      reports: ['json'],
      fixWebpackSourcePaths: true,
      combineBrowserReports: true,
      'report-config': {
        json: {
          file: `coverage-${projectName}.json`
        }
      }
    },
    junitReporter: {
      outputDir: path.join(reportDir, 'junit'),
      outputFile: `junit-${projectName}.xml`
    },
    reporters: ['junit', 'coverage-istanbul', 'spec'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadless'],
    singleRun: false,
    restartOnFileChange: true,
    customLaunchers: {
      ChromeHeadlessDocker: {
        name: 'ChromeHeadlessDocker',
        base: 'ChromeHeadless',
        flags: ['--no-sandbox', '--disable-web-security', '--disable-gpu']
      }
    }
  });
}

/**
 * Export the configuration for the base project.
 */
module.exports = function (config) {
  const handler = configFactory('ui');
  return handler(config);
};

/**
 * Attach the factory so other projects can use it.
 */
module.exports.factory = configFactory;
