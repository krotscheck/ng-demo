// @ts-check
// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');
const { JUnitXmlReporter } = require('jasmine-reporters');
const path = require('path');

const xmlReporter = new JUnitXmlReporter({
  consolidateAll: true,
  savePath: path.resolve(__dirname, '..', 'reports', 'e2e')
});
const specReporter = new SpecReporter({ spec: { displayStacktrace: true } });

/**
 * @type { import("protractor").Config }
 */
exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    './src/**/*.e2e-spec.ts'
  ],
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: [
        '--window-size=1024,768'
      ]
    }
  },
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function () {
    }
  },
  onPrepare() {
    require('ts-node').register({
      project: path.join(__dirname, './tsconfig.json')
    });
    jasmine.getEnv().addReporter(xmlReporter);
    jasmine.getEnv().addReporter(specReporter);
  }
};
