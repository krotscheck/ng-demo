// @ts-check
// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const baseConfig = require('./protractor.conf');

/**
 * @type { import("protractor").Config }
 */
exports.config = Object.assign({}, baseConfig.config, {
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: [
        '--headless',
        '--window-size=1024,768',
        '--no-sandbox',
        '--disable-web-security',
        '--disable-gpu'
      ]
    }
  }
});
